import { Component, OnInit } from '@angular/core';
import { staticsData } from '../../shared/statics-data';
import { AdmService } from '../../_services/adm.service';
@Component({
	selector: 'add-club',
	templateUrl: 'add-club.component.html',
	providers: [AdmService]
})

export class AddClubComponent implements OnInit {
	constructor(private adminService: AdmService) { }
	ngOnInit() { }
	newClub(formValue: any) {
		console.log(formValue.value);


	this.adminService.createNewClub(formValue.value).subscribe(data => {
			console.log(data);
		},
			error => {
				console.log(error);
			});
	
	}

	formState(event) {
		console.log(event.target.value);


	}
}