import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AdministratorComponent } from './administrator.component';
import { AdministratorRoutingModule } from './administrator.routing';
import { AllCourtsComponent } from './all-courts/all-courts.component';
import { AllClubsComponent } from './all-clubs/all-clubs.component';

import { AddClubComponent } from './add-club/add-club.component';
import { AddCourtComponent } from './add-court/add-court.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ChartsModule,
    AdministratorRoutingModule,
    HttpModule
  ],
  declarations: [
    AdministratorComponent,
    AllCourtsComponent,
    AllClubsComponent,
    AddClubComponent,
    AddCourtComponent
    ]
})
export class AdministratorModule { }