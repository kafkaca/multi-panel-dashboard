import { TestBed, inject } from '@angular/core/testing';

import { AllClubsComponent } from './all-clubs.component';

describe('a all-clubs component', () => {
	let component: AllClubsComponent;

	// register all needed dependencies
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				AllClubsComponent
			]
		});
	});

	// instantiation through framework injection
	beforeEach(inject([AllClubsComponent], (AllClubsComponent) => {
		component = AllClubsComponent;
	}));

	it('should have an instance', () => {
		expect(component).toBeDefined();
	});
});