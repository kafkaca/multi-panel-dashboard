import { Component, OnInit } from '@angular/core';
import { AdmService } from '../../_services/adm.service';
@Component({
	selector: 'all-clubs',
	templateUrl: 'all-clubs.component.html'
})

export class AllClubsComponent implements OnInit {

corts : any = [];
	constructor(private adminService: AdmService) { }

	ngOnInit() {
		this.adminService.getAllClubs().subscribe(resp => {
			this.corts = resp.data.result;
			console.log(this.corts);
			
		},
			error => {
				console.log(error);
			});
	}
}